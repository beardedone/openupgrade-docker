FROM python:3

ARG ODOO_VERSION

RUN apt-get update && \
    apt-get install --no-install-recommends -y git python3-dev libxml2-dev libxslt-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN git clone --branch $ODOO_VERSION https://github.com/OCA/OpenUpgrade.git /openupgrade
RUN pip install git+git://github.com/OCA/openupgradelib.git

WORKDIR /openupgrade

# RUN pip install -r requirements.txt
